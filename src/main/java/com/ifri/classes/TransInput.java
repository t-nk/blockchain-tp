package com.ifri.classes;

public class TransInput {
	public String transOutputId;
	public TransOutput utxo;
	
	public TransInput(String transOutputId) {
		super();
		this.transOutputId = transOutputId;
	}

	@Override
	public String toString() {
		return "{\"transOutputId\":\"" + transOutputId + "\", \"utxo\":\"" + utxo + "}";
	}
}
