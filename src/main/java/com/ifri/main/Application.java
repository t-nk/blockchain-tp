package com.ifri.main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Base64;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ifri.classes.*;

import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.ifri" })
@EntityScan(basePackages = { "com.ifri.models" })
@EnableJpaRepositories(basePackages = { "com.ifri.repositories" })
public class Application {
	public static Wallet walletA;
	public static Wallet walletB;
	public static Wallet walletC;
	
	public static int difficulty = 3;
	public static Transaction firsTransaction;
	public static ArrayList<Block> blockchain = new ArrayList<Block>();
	public static HashMap<String, TransOutput> listUTXO = new HashMap<String, TransOutput>();
	//public static ArrayList<Wallet> wallets = new ArrayList<Wallet>();

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
		
		System.out.println("ok !!!");
		
		
		//new wallets
		/*
		walletA = new Wallet();
		walletB = new Wallet();
		walletC = new Wallet();
		
		// new transaction: walletA send funds to walletB
		//add transaction
		firsTransaction = new Transaction(walletC.getPublicKey(), walletB.getPublicKey(), 1000F, null);
		firsTransaction.sign(walletC.getPrivateKey());
		firsTransaction.transactionId="0";
		firsTransaction.transOutput.add(new TransOutput(firsTransaction.transactionId, firsTransaction.receiver, firsTransaction.value));
		listUTXO.put(firsTransaction.transOutput.get(0).id, firsTransaction.transOutput.get(0));
		
		Block firstBlock = new Block("0");
		firstBlock.addTransaction(firsTransaction);
		firstBlock.mining(difficulty);
		
		blockchain.add(firstBlock); //add first block to the blockchain
		//System.out.println("Block 0 : " + blockchain.get(0).toString());
		System.out.println("wallet B : " + walletB.calculateBalance());
		System.out.println("====================================");
		
		
		Block seconBlock = new Block(firstBlock.hash);
		seconBlock.addTransaction(walletB.sendMoney(walletA.getPublicKey(), 300F));
		firstBlock.mining(difficulty);
		blockchain.add(seconBlock);
		
		System.out.println("wallet B : " + walletB.calculateBalance());
		System.out.println("wallet A : " + walletA.calculateBalance());
		*/
		
		
		
		
		/*
		Blockchain.add(new Block("Premier block","10000"));
		Blockchain.get(0).mining(difficulty);
		String[] donnees = {"0","1","2","3","5","6","7","8","9","10","11","12","13","14","15"};
		for(int i = 0;i< donnees.length;i++) {
			Blockchain.add(new Block( Blockchain.get(Blockchain.size()-1).getHash(),donnees[i]));
			//Blockchain.get(Blockchain.size()-1).mining(difficulty);
			//System.out.println(StringUtil.getJson( Blockchain.get(Blockchain.size()-1)));
		}*/
		
		//check the bloc validity
		/*System.out.println("Avant : " + validateBlockchain());
		Blockchain.get(2).setData("56");
		System.out.println("Après : " + validateBlockchain());*/
		
		//test du wallet
		/*
		w1 = new Wallet();
		w2 = new Wallet();
		w3 = new Wallet();
		
		firstTrans = new Transaction(w1.getPublicKey(),w2.getPublicKey(),100F,null);
		firstTrans.sign(w1.getPrivateKey());
		firstTrans.transactionId = "o";
		firstTrans.transOutput.add(new TransOutput(firstTrans.transactionId, firstTrans.receiver, firstTrans.value));
		listUTXO.put(firstTrans.transOutput.get(0).id, firstTrans.transOutput.get(0));
		Block b0 = new Block("0");
		b0.addTransaction(firstTrans);
		b0.mining(difficulty);
		
		blockchain.add(b0);
		
		for(int i = 0;i< blockchain.size();i++) {
			System.out.println(StringUtil.getJson( blockchain.get(blockchain.size()-1)));
		}*/
		
		//w1 send founds to w2
		/*
		System.out.println("============================================================================  ");	
		Transaction trans1 = new Transaction(w1.getPublicKey(),w2.getPublicKey(), 10.0F, null);
		trans1.sign(w1.getPrivateKey());
		System.out.println("trans1 Sign  == " + Base64.toBase64String(trans1.signature));
		System.out.println("trans1.checkSign  == " + trans1.checkSign()); */
	}

	//methods for read, write and delete in file
	public static boolean fileExist (String filename) throws IOException {
		File f = new File(filename+".json");
		if(f.exists() && !f.isDirectory()) { 
		    return true;
		}else {
			return false;
		}
	}
	
	public static void writeJson(String filename, JSONArray array) throws Exception {
		JSONObject datas = new JSONObject();
	    datas.put(filename, array);
	    Files.write(Paths.get(filename+".json"), datas.toJSONString().getBytes());
	    System.out.println("write; " + filename );
	}
	
	public static Object readJson(String filename) throws Exception {
		FileReader reader = new FileReader(filename+".json");
	    JSONParser jsonParser = new JSONParser();
	    System.out.println("read; " + filename);
		return jsonParser.parse(reader);
	}
	
	public static JSONArray getDatasFromFile(String filename) throws IOException, Exception {
		if(fileExist(filename)) {
			JSONObject object = (JSONObject) readJson(filename);
			JSONArray array = (JSONArray) object.get(filename);
    		if(array.size() > 0)
    			return array;
    		return null;
    	}else {
    		return null;
    	}
	}
	
	public static boolean deleteFile(String filename) {
		File file = new File(filename+".json");
        if(file.delete())
        {
            return true;
        }
        else
        {
            return false;
        }
	}
	
	//get private and public keys
	public static PrivateKey getPrivateKey(String key) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
		if(key != null) {
			System.out.println("privKey String : "+ key);
			byte[] privateKeyBytes = Base64.getDecoder().decode(key);
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("ECDSA","BC");
			PrivateKey priKey = keyFactory.generatePrivate(keySpec);
			return priKey;
		}
		return null;
	}
	
	public static PublicKey getPublicKey(String key) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
		if(key != null) {
			System.out.println("pubKey String : "+ key);
			byte[] publicKeyBytes = Base64.getDecoder().decode(key);
			X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("ECDSA","BC");
			//KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA","BC"); 
			PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);
			return pubKey;
		}
		return null;
	}
	
	//methods for listUTXO
	public static boolean updateListUTXOVariable() throws IOException, Exception {
		listUTXO.clear();
		JSONArray transactionsutxo = Application.getDatasFromFile("transactionsutxo");
		if(transactionsutxo != null && transactionsutxo.size() > 0) {
			for (Object o : transactionsutxo)
			{
				//System.out.println("utxo : "+ o);
				JSONObject t = (JSONObject) o;
				TransOutput trans = new TransOutput();
				trans.parentTransactionId = (String) t.get("parenttransactionId");
				trans.id = (String) t.get("id");
				trans.recipient = getPublicKey((String)t.get("recipient"));
				trans.value = ((Double) t.get("value")).floatValue();
				listUTXO.put(trans.id, trans);
			}
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean updateListUTXOFIle() throws Exception {
		JSONArray transactionsutxo = new JSONArray();
		for(Map.Entry<String, TransOutput> item: listUTXO.entrySet()){	
			TransOutput utxo = item.getValue();
			JSONObject transOutput = new JSONObject();
			
			transOutput.put("id",utxo.id);
			transOutput.put("parentTransactionId",utxo.parentTransactionId);
			transOutput.put("recipient", Base64.getEncoder().encodeToString(utxo.recipient.getEncoded()));
			transOutput.put("value",utxo.value);
			transactionsutxo.add(utxo);
		}
		Application.writeJson("transactionsutxo",transactionsutxo);
		return true;
	}
	
	//methods for blockchain
	public static boolean updateBlockchainVariable() throws IOException, Exception {
		blockchain.clear();
		JSONArray blocks = Application.getDatasFromFile("blocks");
		if(blocks != null && blocks.size() > 0) {
			for (Object o : blocks)
			{
				//System.out.print("block : "+ o);
				JSONObject b = (JSONObject) o;
				//System.out.print("the current block file" + b);
				Block block = new Block();
	    		block.hash = (String)b.get("hash");
	    		block.setPreviousHash((String)b.get("previousHash"));
	    		block.setTimestamp(Long.valueOf((String) b.get("timestamp")));
	    		block.setNounce(((Long) b.get("nounce")).intValue());
	    		block.transactions = (ArrayList<Transaction>)b.get("transactions");
	    		block.setMerkleRoot((String)b.get("merkleRoot"));
	    		
				blockchain.add(block);
			}
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean updateBlockchainFIle() throws Exception {
		JSONArray blocks = new JSONArray();
		for(Block b: blockchain){
			JSONObject block = new JSONObject();
			block.put("hash", b.hash);
    		block.put("previousHash", b.getPreviousHash());
    		block.put("timestamp", b.getTimestamp() );
    		block.put("nounce", b.getNounce());
    		block.put("transactions", b.transactions);
    		block.put("merkleRoot", b.getMerkleRoot());
			blocks.add(b);
		}
		Application.writeJson("blocks",blocks);
		return true;
	}


}
