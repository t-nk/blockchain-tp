package com.ifri.controllers;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ifri.classes.*;
import com.ifri.main.Application;
import com.ifri.models.Admin;
import com.ifri.models.Transfert;

@Controller
@RequestMapping("/shop")
public class ShopController {
	@RequestMapping(value = "/View")
	public String View(Model model) {
		return "shop";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/buy")
	public String buy(@RequestParam String name,float price,String image, Model model) throws IOException, Exception {
		//System.out.println("name: " + name + "price: " + price + "image: " + image );

		//get listUTXO from file to Application.listUTXO
		Application.updateListUTXOVariable();
		for(Map.Entry<String, TransOutput> item: Application.listUTXO.entrySet()){	
			System.out.println("one transaction utxo : " + item.getValue());
		}
		
		JSONArray products = Application.getDatasFromFile("products");
		JSONArray infos = Application.getDatasFromFile("infos");
		JSONObject info  = (JSONObject) infos.get(0);
		
		Wallet wallet = new Wallet();
		
		wallet.setFullname((String)info.get("fullname"));
		wallet.setEmail((String)info.get("email"));
		wallet.setPassword((String)info.get("password"));
		wallet.setPrivateKey( (PrivateKey) Application.getPrivateKey((String)info.get("privateKey")) );
		wallet.setPublicKey( (PublicKey)Application.getPublicKey((String)info.get("publicKey")));
		//wallet.listUTXOWallet = (HashMap<String, TransOutput>)info.get("listUTXOWallet");
		
		if(wallet.calculateBalance() >= price) {
			
			//get public key of the admin
			JSONObject adminDatas = (JSONObject) Application.readJson("admin");
			PublicKey pubKeyAdmin = Application.getPublicKey((String)adminDatas.get("publicKey"));
			//the new transaction for payment
			Transaction transaction = wallet.sendMoney(pubKeyAdmin, price);
			
	        //get blockchain from file to Application.blockchain
	      	Application.updateBlockchainVariable();
	      	
	        if(Application.blockchain.size() > 0) { 
	    		Block currentBlock = Application.blockchain.get(Application.blockchain.size()-1);
	    		
	    		//check the limit of 10 transactions by block
	    		if(currentBlock.transactions.size() < 10) {
	    			currentBlock.addTransaction(transaction);
	    			currentBlock.mining(Application.difficulty);
	    			Application.blockchain.set((Application.blockchain.size()-1), currentBlock);
	    		}else {
	    			Block block = new Block(currentBlock.getPreviousHash());
	        		block.addTransaction(transaction);
	        		block.mining(Application.difficulty);
	        		Application.blockchain.add(block);
	    		}
	    	}else {
	    		System.out.println("transaction: " + transaction);

	    		//if no blocks in the array
	    		Block block = new Block("0");
	    		System.out.println("blockA: " + block);
	    		block.addTransaction(transaction);
	    		block.mining(Application.difficulty);
	    		System.out.println("blockB: " + block);
	    		Application.blockchain.add(block);
	    	}
	        //update blockchain from Application.blockchain to file
	        Application.updateBlockchainFIle();
	        //update listUTXO from Application.listUTXO to file
	        Application.updateListUTXOFIle();
			
			//add the new product buy
			if(products != null && products.size() > 0) {
				
				JSONObject product = new JSONObject();
				product.put("client", info.get("publicKey"));
				product.put("name", name);
				product.put("price", price);
				product.put("image", image);
				
				products.add(product);
				Application.writeJson("products", products);
			}else {
				JSONArray datas = new JSONArray();
				
				JSONObject product = new JSONObject();
				product.put("client", info.get("publicKey"));
				product.put("name", name);
				product.put("price", price);
				product.put("image", image);
				
				datas.add(product);
				Application.writeJson("products", datas);	
			}
			return "redirect:/shop/View";
		}else {
      		model.addAttribute("msg", "Vous n\'avez pas assez de fonds !!!");
    		return "shop";
      		//return "shop";
      	}
    }
}
