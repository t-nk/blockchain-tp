package com.ifri.controllers;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ifri.classes.Block;
import com.ifri.classes.TransOutput;
import com.ifri.classes.Transaction;
import com.ifri.classes.Wallet;
import com.ifri.main.Application;
import com.ifri.models.Game;

@Controller
@RequestMapping("/game")
public class GameController {
	int N = 0;
	int nbre = 5;
	String msg = "";
	
	@RequestMapping(value = "/View")
	public String View(Model model) {
		if(nbre == 5) {
			N = new Integer(new java.util.Random().nextInt(101));	
			System.out.println("N : "+N);
		}
		
		model.addAttribute("nbre", this.nbre);
		model.addAttribute("msg", this.msg);
		return "game";
	}
	
	@RequestMapping(value = "/game")
	public String game(@RequestParam(required=false) int nombre,Model model) throws IOException, Exception{
		if(nbre == 1) {
			int lastN = N;
			N = new Integer(new java.util.Random().nextInt(101));
			nbre = 5;
			String msg = "Plus de tentatives. Vous avez perdu !!! Le nombre mystère était : " + N;
			model.addAttribute("msg", msg);
			model.addAttribute("nbre", 5);
			//model.addAttribute("game", game);
			return "game";
		}else {
			if(nombre > N) {
				msg = "Votre nombre est supérieur au nombre mystère";
				nbre -= 1;
			}
			if(nombre < N) {
				msg = "Votre nombre est inférieur au nombre mystère";
				nbre -= 1;
			}
			if(nombre == N) {
				//get listUTXO from file to Application.listUTXO
				Application.updateListUTXOVariable();
				
				//get public key of the admin
				JSONObject adminDatas = (JSONObject) Application.readJson("admin");
				PublicKey pubKeyAdmin = Application.getPublicKey((String)adminDatas.get("publicKey"));
				//get currentUser datas
				JSONArray infos = Application.getDatasFromFile("infos");
				JSONObject info  = (JSONObject) infos.get(0);
				
				Wallet wallet = new Wallet();
				
				wallet.setFullname((String)info.get("fullname"));
				wallet.setEmail((String)info.get("email"));
				wallet.setPassword((String)info.get("password"));
				wallet.setPrivateKey( (PrivateKey) Application.getPrivateKey((String)info.get("privateKey")) );
				wallet.setPublicKey( (PublicKey)Application.getPublicKey((String)info.get("publicKey")));
				//wallet.listUTXOWallet = (HashMap<String, TransOutput>)info.get("listUTXOWallet");
				
				//build the new transaction
				Transaction transaction = new Transaction(pubKeyAdmin, wallet.getPublicKey(), 100F, null);
				transaction.sign(Application.getPrivateKey((String)adminDatas.get("privateKey")));
		    	if(Application.listUTXO.size() > 0) {
		    		transaction.transactionId = String.valueOf(Application.listUTXO.size()+1);
		    	}else {
		    		transaction.transactionId = String.valueOf(1);
		    	}
				transaction.transOutput.add(new TransOutput(transaction.transactionId, transaction.receiver, transaction.value));
				
		        //get blockchain from file to Application.blockchain
		      	Application.updateBlockchainVariable();
		        
		      	if(Application.blockchain.size() > 0) { 
		    		Block currentBlock = Application.blockchain.get(Application.blockchain.size()-1);
		    		
		    		//check the limit of 10 transactions by block
		    		if(currentBlock.transactions.size() < 10) {
		    			currentBlock.addTransaction(transaction);
		    			currentBlock.mining(Application.difficulty);
		    			Application.blockchain.set((Application.blockchain.size()-1), currentBlock);
		    		}else {
		    			Block block = new Block(currentBlock.getPreviousHash());
		        		block.addTransaction(transaction);
		        		block.mining(Application.difficulty);
		        		Application.blockchain.add(block);
		    		}
		    	}else {
		    		System.out.println("firstTransaction: " + transaction);

		    		//if no blocks in the array
		    		Block block = new Block("0");
		    		System.out.println("blockA: " + block);
		    		block.addTransaction(transaction);
		    		block.mining(Application.difficulty);
		    		System.out.println("blockB: " + block);
		    		Application.blockchain.add(block);
		    	}
		        //update blockchain from Application.blockchain to file
		        Application.updateBlockchainFIle();
		        //update listUTXO from Application.listUTXO to file
		        Application.updateListUTXOFIle();
				
				nbre = 5;
				N = new Integer(new java.util.Random().nextInt(101));
				msg = "Félicitations !!! Vous avez gagné 100 UAC !!! ";
			}
					
			model.addAttribute("nbre", nbre);
			model.addAttribute("msg", msg);
			return "game";
		}
    }
}
