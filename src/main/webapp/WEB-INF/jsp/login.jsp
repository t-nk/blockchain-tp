<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<script src="/js/jquery-3.2.1.slim.min.js"></script>
		<script src="/js/popper.js/1.11.0/umd/popper.min.js"></script>
		
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<!-- Line awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    	integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />		<!-- Custom styles for this template -->
		<!-- jquery -->
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		    	integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
		
		<!-- datatable -->
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		
		<!-- datatable css -->
		<link rel="stylesheet" type="text/css"
		    href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
		 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
    		integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
    		crossorigin="anonymous"></script>
    
		<title>uaCCash</title>
		
		<style>
			.card {
		        margin: 0 auto; /* Added */
		        float: none; /* Added */
		        margin-bottom: 10px; /* Added */
			}
		</style>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="/">CareHome</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		</nav>
		


		<div class="container h-100">
		    
			<div class="align-center">
				<div class="card " style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title text-center" >Connexion</h5>
						
						<form class="mt-5" method="POST" action="/login" modelAttribute="admin" >
							<div class="form-group">
								<label for="exampleInputEmail1">Adresse Email</label>
								<input type="email" name="email"  class="form-control">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Mot de Passe</label>
								<input type="password" name="password"  class="form-control">
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary text-center">Soumettre</button>
							</div>
						</form>
						<div class="text-center mt-5">
							<a href="/registerView"><small>Vous n'avez pas de compte ? <br> Veuillez vous inscrire.</small> </a>
						</div>
					</div>
				</div>
			</div>


		</div>



	</body>
</html>