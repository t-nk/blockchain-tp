package com.ifri.classes;

import java.security.PublicKey;
import java.util.Base64;

import com.ifri.main.Application;

public class TransOutput {
	public String id;
	public String parentTransactionId;
	public PublicKey recipient;
	public float value;
	
	public TransOutput() {
		super();
	}
	
	public TransOutput(String parentTransactionId, PublicKey recipient, float value) {
		super();
		this.parentTransactionId = parentTransactionId;
		this.recipient = recipient;
		this.value = value;
		String dataInput = StringUtil.getStringFromKey(recipient)+parentTransactionId+Float.toString(this.value);
		this.id = StringUtil.applySha256(dataInput);
	}
	
	//pour vérifier qu'une transaction a pour destinataire un wallet précis
	public boolean isMine(PublicKey myKey){
        return (myKey == recipient);
    }

	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\", \"parentTransactionId\":\"" + parentTransactionId + "\", \"recipient\":\""+ Base64.getEncoder().encodeToString(recipient.getEncoded()) + "\", \"value\":" + value + "}";
	}
}
