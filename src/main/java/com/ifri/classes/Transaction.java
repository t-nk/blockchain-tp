package com.ifri.classes;

import java.io.IOException;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.ifri.main.Application;


// UTXO : Unspent Transaction Output
public class Transaction {
	public String transactionId;
	public PublicKey sender;
	public PublicKey receiver;
	public float value;
	public Long timestamp;
	public ArrayList<TransInput> transInput = new ArrayList<TransInput>();
	public ArrayList<TransOutput> transOutput = new ArrayList<TransOutput>();
	public byte[] signature;
	
	public Transaction(PublicKey sender, PublicKey receiver, float value, ArrayList<TransInput> transInput ) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.value = value;
		this.timestamp = new Date().getTime();
		this.transInput =  transInput;
	}
	
	//function for calculate hash
	//pour calculer le hash avec les différents parmaètres passé à la fonction
	public String calculateHash() {
		//sender receiver value timestamp
		return StringUtil.applySha256( StringUtil.getStringFromKey(sender)+StringUtil.getStringFromKey(receiver)+Double.toString(this.value)+Long.toString(this.timestamp));
	}
	
	//function for sign
	//pour signer une transaction, elle prend la clé privée de celui qui effectue l'envoie et retourne la signature
	public void sign(PrivateKey privateKey) {
		String dataInput = StringUtil.getStringFromKey(sender)+StringUtil.getStringFromKey(receiver)+Double.toString(this.value)+Long.toString(this.timestamp);
		this.signature = StringUtil.applyECDSASig(privateKey, dataInput);
	}
	
	//function for check sign
	//pour vérifier que la signature d'une transactione est correcte
	public boolean checkSign() {
		String dataInput = StringUtil.getStringFromKey(sender)+StringUtil.getStringFromKey(receiver)+Double.toString(this.value)+Long.toString(this.timestamp);
		return StringUtil.verifyECDSASig(sender, dataInput, this.signature);
	}
	
	//pour traiter une transaction puis l'ajouter au block souhaité
	 public boolean treatTransaction() throws IOException, Exception{
		float leftOver = 0;
		 
        if(!checkSign()){
            return false;
        }
        // check if inputs are utxo
        if(this.transInput != null) {
        	for(TransInput transactionInput: this.transInput){
	            transactionInput.utxo = Application.listUTXO.get(transactionInput.transOutputId);
	        }
        	leftOver = calculateSumValueInputs() - value;
        }
        
        //calculate hash
        transactionId = calculateHash();
        //clear transactions output list
        this.transOutput.clear();
        // generating outputs
        this.transOutput.add(new TransOutput(transactionId, receiver, value));
        this.transOutput.add(new TransOutput(transactionId, sender, leftOver));
        // remove inputs from Masterblockchain.listUTXO
        if(this.transInput != null) {
        	for(TransInput transactionInput: transInput){
                if(transactionInput.utxo == null) {
                    continue;
                }
                Application.listUTXO.remove(transactionInput.transOutputId);
            }
        }
        // add new outputs to Masterblockchain.listUTXO
        for(TransOutput transOutput: transOutput){
        	//System.out.println("new trans Output: " + transOutput);
        	Application.listUTXO.put(transOutput.id, transOutput);
        }
       
        return true;
    }
 	
 	//pour calculer la valeurs des inputs d'une transactions 
    public float calculateSumValueInputs(){
        float somme = 0;
        for(TransInput transactionInput: transInput){
            if(transactionInput.utxo == null){
                continue;
            }
            somme += transactionInput.utxo.value;
        }
        return somme;
    }

	@Override
	public String toString() {
		return "{\"transactionId\":\"" + transactionId + "\", \"sender\":\"" + Base64.getEncoder().encodeToString(sender.getEncoded()) 
				+ "\", \"receiver\":\"" + Base64.getEncoder().encodeToString(receiver.getEncoded())+ "\", \"value\":" + value 
				+ ", \"timestamp\":\"" + timestamp + "\", \"transInput\":" 
				+ transInput+ ", \"transOutput\":"+ transOutput + ", \"signature\":\"" + Base64.getEncoder().encodeToString(signature) + "\"}";
	}
}
