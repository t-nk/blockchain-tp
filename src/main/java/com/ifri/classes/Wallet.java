package com.ifri.classes;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.ifri.main.Application;


public class Wallet {
	private String fullname;
	private String email;
	private String password;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	public HashMap<String, TransOutput> listUTXOWallet = new HashMap<String, TransOutput>();
	
	
	public Wallet() {
		super();
		this.generateKeyPair();
	}
	
	
	
	public Wallet(String fullname, String email, String password, PrivateKey privateKey, PublicKey publicKey,
			HashMap<String, TransOutput> listUTXOWallet) {
		super();
		this.fullname = fullname;
		this.email = email;
		this.password = password;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
		this.listUTXOWallet = listUTXOWallet;
	}

	public Wallet(String fullname, String email, String password) {
		super();
		this.generateKeyPair();
		this.fullname = fullname;
		this.email = email;
		this.password = password;
	}

	//generate key
	//pour générer les clés privée et public lors de la création du wallet
	public void generateKeyPair() {
		try {
			Security.addProvider(new BouncyCastleProvider());
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA","BC");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");
			// Initialize the key generator and generate a KeyPair
			keyGen.initialize(ecSpec, random); //256 
	        KeyPair keyPair = keyGen.generateKeyPair();
	        // Set the public and private keys from the keyPair
	        privateKey = keyPair.getPrivate();
	        publicKey = keyPair.getPublic();
	        
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	//pour calculer les solde du wallet
	public void chargeUTXOWallet(){
        for(Map.Entry<String, TransOutput> item: Application.listUTXO.entrySet()){
        	if(item.getValue().recipient.equals(publicKey)){
                listUTXOWallet.put(item.getKey(), item.getValue());
            }
        }
    }
	
	//calculate balance
	//pour calculer les solde du wallet
	public float calculateBalance(){
        float balance = 0;
        for(Map.Entry<String, TransOutput> item: Application.listUTXO.entrySet()){
        	if(item.getValue().recipient.equals(publicKey)){//if(item.getValue().isMine(publicKey)){
        		//System.out.println("new utxo add to the localWallet: " + item.getValue());
        		//System.out.println("value: " + item.getValue().value);
        		listUTXOWallet.put(item.getKey(), item.getValue());
                balance += item.getValue().value;
            }
        }
        return balance;
    }
	
	//send money
	//pour envoyer de l'argent à un autre wallet
	public Transaction sendMoney(PublicKey recipient,float value){
		if(this.calculateBalance() < value ) {
			System.out.print("Solde insuffisant !!!");
			return null;
		}
		ArrayList<TransInput> inputs = new ArrayList<TransInput>();
		float sumCompiled = 0;
		for(Map.Entry<String, TransOutput> item: this.listUTXOWallet.entrySet()){	
			TransOutput utxo = item.getValue();
			sumCompiled += utxo.value;
			inputs.add(new TransInput(utxo.id));
			this.listUTXOWallet.remove(item.getKey());
            if(sumCompiled >= value){
                break;
            }
		}
		Transaction newTrans = new Transaction(this.publicKey,recipient,value,inputs);
		newTrans.sign(this.privateKey);
		return newTrans;
	}
	
	//getteurs and setteurs
	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "{\"fullname\":\"" + fullname + "\", \"email\":\"" + email + "\", \"password\":\"" + password + "\", \"privateKey\":\""
				+ Base64.getEncoder().encodeToString(privateKey.getEncoded()) + "\", \"publicKey\":\"" + Base64.getEncoder().encodeToString(publicKey.getEncoded())+
						"\", \"listUTXOWallet\":" + listUTXOWallet + "}";
	}
}
