package com.ifri.classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class Block {
	public String hash;
	private String previousHash;
	//private String data;
	private long timestamp;
	private int nounce;
	public ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	private String merkleRoot; 
	
	
	public Block() {
		super();
	}
	
	public Block(String previousHash) {
		super();
		this.previousHash = previousHash;
		this.timestamp = new Date().getTime();
		this.calculateHash();
	}
	
	//function for calculate
	public String calculateHash() {
		return this.hash = StringUtil.applySha256(previousHash+Long.toString(timestamp)+Integer.toString(nounce)+merkleRoot);
	}
	
	//function for mining
	// miner le block, trouver un hash au block qui respecte le niveau de difficulté exigé
	public String mining(int difficulty){
		System.out.println("--------------------------------------------");
		System.out.println("Minage en cours");
		String difficultyString = StringUtil.getDificultyString(difficulty);
		
		while(!this.hash.substring(0,difficulty).equals(difficultyString)) {
			this.nounce ++;
			//System.out.println("Try for nounce : " + this.nounce);
			this.calculateHash();
		};
		System.out.println("Minaga OK");
		System.out.println("--------------------------------------------");
		return this.hash;
	}
		
	//function for add transaction
	//pour ajouter une transaction au block, une opération d'envoie de sous d'un wallet à un autre par exemple
	public boolean addTransaction(Transaction trans ) throws IOException, Exception {
		if(trans == null) return false;
		if(trans.transactionId != "0") {
			if(trans.treatTransaction() == false) return false;
			this.transactions.add(trans);
			return true;
		}
		return false;
	}

	//accesseurs
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public void setPreviousHash(String previousHash) {
		this.previousHash = previousHash;
	}
	
	public int getNounce() {
		return nounce;
	}

	public void setNounce(int nounce) {
		this.nounce = nounce;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getMerkleRoot() {
		return merkleRoot;
	}

	public void setMerkleRoot(String merkleRoot) {
		this.merkleRoot = merkleRoot;
	}

	@Override
	public String toString() {
		return "{\"hash\":\"" + hash + "\", \"previousHash\" :\"" + previousHash + "\", \"timestamp\":\"" + timestamp + "\", \"nounce\":"
				+ nounce + ", \"transactions\":" + transactions + ",\" merkleRoot\":\"" + merkleRoot + "\"}";
	}
}
