package com.ifri.controllers;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Map;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ifri.classes.TransOutput;
import com.ifri.classes.Wallet;
import com.ifri.main.Application;

@Controller
@RequestMapping("/wallet")
public class WalletController {
	@RequestMapping(value = "/View")
	public String View(Model model) throws IOException, Exception {
		//get listUTXO from file to Application.listUTXO
		Application.updateListUTXOVariable();
		for(Map.Entry<String, TransOutput> item: Application.listUTXO.entrySet()){	
			System.out.println("one transaction utxo : " + item.getValue());
		}
		
		float solde = 0;
		JSONArray infos = Application.getDatasFromFile("infos");
		JSONObject info  = (JSONObject) infos.get(0);
		System.out.println("info : " + info);
		Wallet wallet = new Wallet();
		
		wallet.setFullname((String)info.get("fullname"));
		wallet.setEmail((String)info.get("email"));
		wallet.setPassword((String)info.get("password"));
		wallet.setPrivateKey( (PrivateKey) Application.getPrivateKey((String)info.get("privateKey")) );
		wallet.setPublicKey( (PublicKey)Application.getPublicKey((String)info.get("publicKey")));
		//wallet.listUTXOWallet = (HashMap<String, TransOutput>)info.get("listUTXOWallet");
		
		//wallet.setPrivateKey(null)
		//wallet.chargeUTXOWallet();
		
		//System.out.println("solde : " + wallet.calculateBalance());
		//System.out.println("wallet : " + wallet);
		
		model.addAttribute("solde", wallet.calculateBalance());
		model.addAttribute("publicKey",  Base64.getEncoder().encodeToString(wallet.getPublicKey().getEncoded()));
		return "wallet";
	}
}