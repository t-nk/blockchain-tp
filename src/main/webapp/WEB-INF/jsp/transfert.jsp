<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<script src="/js/jquery-3.2.1.slim.min.js"></script>
		<script src="/js/popper.js/1.11.0/umd/popper.min.js"></script>
		
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<!-- Line awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    	integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />		<!-- Custom styles for this template -->
		<!-- jquery -->
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		    	integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
		
		<!-- datatable -->
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		
		<!-- datatable css -->
		<link rel="stylesheet" type="text/css"
		    href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
		 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
    		integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
    		crossorigin="anonymous"></script>
    
		<title>uaCCash</title>

		<style>
			a { color: inherit; } 
		</style>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="/home">uaCCash</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <!-- 
				<li class="nav-item ">
		        <a class="nav-link" href="/listeV">Nos véhicules</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="/listeL">Vos locations</a>
		      </li>
			   -->
		    </ul>
		    
		    <ul class="navbar-nav pull-right">
		      <li class="nav-item ">
		        <a class="nav-link" href="/logout">Déconnexion</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		
		<div class="container h-100 ">
		   <h1 class="text-center mt-3"> TRANSFERT DE UACToken</h1>
					<div class=" mt-5 align-center d-flex justify-content-center">
						<form class="mt-5" method="POST" action="/transfert/treat"  >
									<div class="mt-5 text-center"> Vous avez <strong> ${solde} </strong> UACToken</div>
									<div class="mt-1 text-center">  Les frais de transaction sont de 3% du montant transféré.</div>
									<div class="form-group mt-5">
											<input type="text" id="recipient" name="recipient" class="form-control" placeholder="Destinataire">
									</div>
									<div class="form-group mt-5">
										<input type="number" min="1" id="value" name="value" class="form-control" placeholder="Montant">
									</div>
									<div class="text-center">
											<button type="submit" class="btn btn-primary text-center">Soumettre</button>
									</div>
									<div class="text-center mt-5">
											<p>${msg}</p>
									</div>
							</form>
					</div>
		</div>
	</body>
</html>