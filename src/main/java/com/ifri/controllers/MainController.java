/**
 * 
 */
package com.ifri.controllers;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Base64;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ifri.main.Application;
import com.ifri.models.*;
import com.google.gson.Gson;
import com.ifri.classes.*;
/**
 * @author t
 *
 */
@Controller
public class MainController {
	JSONParser parser = new JSONParser();
	
	@RequestMapping(value = "/")
	public String index() {
		return "index";
	}
	
	@RequestMapping(value = "/home")
	public String home() {
		return "home";
	}
	
	@RequestMapping(value = "/loginView")
	public String loginView(Model model) {
		Admin admin = new Admin();
		model.addAttribute("admin", admin);
		return "login";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/login")
	public String login(@ModelAttribute Admin admin) throws IOException, Exception {
		JSONArray array = Application.getDatasFromFile("wallets");
		if(array != null) {
			for (Object o : array)
			{
    			JSONObject w = (JSONObject) o;
    			//System.out.println("PrivteKey : " + Application.getPrivateKey((String) w.get("privateKey")));
    			//System.out.println("PubKey : " + Application.getPublicKey((String) w.get("publicKey")));
			    if(w.get("email").equals(admin.getEmail()) && w.get("password").equals(admin.getPassword())) {
			    	JSONArray datas = new JSONArray();
			    	datas.add(o);
					Application.writeJson("infos",datas);
			    	return "redirect:/home";
			    }
			}
    		return "redirect:/";
    	}else {
    		return "redirect:/registerView";
    	}
    }
	
	@RequestMapping(value = "/logout")
	public String logout(Model model) throws Exception {
		Admin admin = new Admin();
		model.addAttribute("admin", admin);
		if(Application.deleteFile("infos")) {
			return "login";
		}
		return "home";
	}
	
	//page register de l'admin
	@RequestMapping(value = "/registerView")
	public String registerView(Model model) {
		Admin admin = new Admin();
		model.addAttribute("admin", admin);
		return "register";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/register")
	public String register(@ModelAttribute Admin admin) throws IOException, Exception {
		//get listUTXO from file to Application.listUTXO
		Application.updateListUTXOVariable();
		
		Wallet wallet = new Wallet(admin.getFullname(),admin.getEmail(),admin.getPassword());
		//get public key of the admin
		JSONObject adminDatas = (JSONObject) Application.readJson("admin");
		PublicKey pubKeyAdmin = Application.getPublicKey((String)adminDatas.get("publicKey"));
		
		//build the new transaction
		Transaction firsTransaction = new Transaction(pubKeyAdmin, wallet.getPublicKey(), 100F, null);
		firsTransaction.sign(Application.getPrivateKey((String)adminDatas.get("privateKey")));
    	if(Application.listUTXO.size() > 0) {
    		firsTransaction.transactionId = String.valueOf(Application.listUTXO.size()+1);
    	}else {
    		firsTransaction.transactionId = String.valueOf(1);
    	}
		firsTransaction.transOutput.add(new TransOutput(firsTransaction.transactionId, firsTransaction.receiver, firsTransaction.value));
		//Application.listUTXO.put(firsTransaction.transOutput.get(0).id, firsTransaction.transOutput.get(0));
		//System.out.println("end with transactionOutput");
		
        //get blockchain from file to Application.blockchain
      	Application.updateBlockchainVariable();
        if(Application.blockchain.size() > 0) { 
    		Block currentBlock = Application.blockchain.get(Application.blockchain.size()-1);
    		
    		//check the limit of 10 transactions by block
    		if(currentBlock.transactions.size() < 10) {
    			currentBlock.addTransaction(firsTransaction);
    			currentBlock.mining(Application.difficulty);
    			Application.blockchain.set((Application.blockchain.size()-1), currentBlock);
    		}else {
    			Block block = new Block(currentBlock.getPreviousHash());
        		block.addTransaction(firsTransaction);
        		block.mining(Application.difficulty);
        		Application.blockchain.add(block);
    		}
    	}else {
    		System.out.println("firstTransaction: " + firsTransaction);

    		//if no blocks in the array
    		Block block = new Block("0");
    		System.out.println("blockA: " + block);
    		block.addTransaction(firsTransaction);
    		block.mining(Application.difficulty);
    		System.out.println("blockB: " + block);
    		Application.blockchain.add(block);
    	}
        //update blockchain from Application.blockchain to file
        Application.updateBlockchainFIle();
        //update listUTXO from Application.listUTXO to file
        Application.updateListUTXOFIle();
        
		//build the new user
		JSONObject user = new JSONObject();
		user.put("fullname", wallet.getFullname());
		user.put("email", wallet.getEmail());
		user.put("password", wallet.getPassword());
		user.put("privateKey", Base64.getEncoder().encodeToString(wallet.getPrivateKey().getEncoded()));
		user.put("publicKey", Base64.getEncoder().encodeToString(wallet.getPublicKey().getEncoded()));
		user.put("listUTXOWallet", wallet.listUTXOWallet);
		
		JSONArray wallets = Application.getDatasFromFile("wallets");
    	if(wallets != null && wallets.size() > 0) {
    		wallets.add(user);
    		Application.writeJson("wallets",wallets);
    		JSONArray infos = new JSONArray();
	    	infos.add(user);
			Application.writeJson("infos",infos);
    		return "redirect:/home";
    	}else {
    		JSONArray datas = new JSONArray();
    	    datas.add(user);
    		Application.writeJson("wallets",datas);
    		JSONArray datas1 = new JSONArray();
	    	datas1.add(user);
			Application.writeJson("infos",datas1);
    		return "redirect:/home";
    	}
    }
}
