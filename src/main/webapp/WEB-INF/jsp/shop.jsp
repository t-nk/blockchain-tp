<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<script src="/js/jquery-3.2.1.slim.min.js"></script>
		<script src="/js/popper.js/1.11.0/umd/popper.min.js"></script>
		
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<!-- Line awesome -->
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    	integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />		<!-- Custom styles for this template -->
		<!-- jquery -->
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		    	integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
		
		<!-- datatable -->
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		
		<!-- datatable css -->
		<link rel="stylesheet" type="text/css"
		    href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
		 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
    		integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
    		crossorigin="anonymous"></script>
    
		<title>uaCCash</title>

		<style>
			a { color: inherit; } 
		</style>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="/home">uaCCash</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <!-- 
				<li class="nav-item ">
		        <a class="nav-link" href="/listeV">Nos véhicules</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="/listeL">Vos locations</a>
		      </li>
			   -->
		    </ul>
		    
		    <ul class="navbar-nav pull-right">
		      <li class="nav-item ">
		        <a class="nav-link" href="/logout">Déconnexion</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		
		<div class="container h-100 ">
		    <h1 class="text-center mt-5 mb-5">uacCoin Shop</h1>
		    
		    <div class="text-center mt-5">
				<p>${msg}</p>
			</div>
            
			<div class="row d-flex justify-content-around" style="margin-top: 30px;">
				<div class="card" style="width: 18rem;">
					<div class="container-fluid"> <img src="/img/goProH7.jpg" class="card-img-top" alt="..." width="120" height="300"></div>
					<div class="card-body">
							<div class="d-flex justify-content-around">
								<h5 class="card-title">Produit : GoPro H7 <br /><strong>Prix : 75 uacT</strong></h5><br />
								<!--h5 class="card-title"><b>Prix : 75 uacT </b></h5-->
							</div>
							<!--  <p class="card-text">Description</p>  -->
							<div class="text-center">
								<a href="/shop/buy?name=GoPro-H7&price=75&image=goProH7"><button class="btn btn-primary text-center">Acheter</button></a>
							</div>
					</div>
				</div>
				<div class="card" style="width: 18rem;">
					<div class="container-fluid"> <img src="/img/battlefield5.jpg" class="card-img-top" alt="..." width="120" height="300"> </div>
					<div class="card-body">
							<div class="d-flex justify-content-around">
								<h5 class="card-title">Produit : Battlefield V <br /><strong>Prix : 35 uacT</strong></h5>
								<!--h5 class="card-title"><b>Prix : 35 uacT</b></h5-->
							</div>
							<!--  <p class="card-text">Description</p>  -->
							<div class="text-center">
								<a href="/shop/buy?name=Battlefield-V&price=35&image=battlefield5"><button class="btn btn-primary text-center">Acheter</button></a>
							</div>
					</div>
				</div>
				<div class="card" style="width: 18rem;">
					<div class="container-fluid"> <img src="/img/oneplus9pro.jpg" class="card-img-top" alt="..." width="120" height="300"></div>
					<div class="card-body">
							<div class="d-flex justify-content-around">
								<h5 class="card-title">Produit : OnePlus 9 Pro <br /><strong>Prix : 250 uacT</strong></h5>
								<!--h5 class="card-title"><b>Prix : 250 uacC</b></h5-->
							</div>
							<!--  <p class="card-text">Description</p>  -->
							<div class="text-center">
								<a href="/shop/buy?name=OnePlus-9-Pro&price=250&image=oneplus9pro"><button class="btn btn-primary text-center">Acheter</button></a>
							</div>
					</div>
				</div>
			</div>
			<div class="row d-flex justify-content-around" style="margin-top: 30px;">
				<div class="card" style="width: 18rem;">
					<div class="container-fluid"> <img src="/img/amazfitGTR.jpg" class="card-img-top" alt="..." width="120" height="300"></div>
					<div class="card-body">
							<div class="d-flex justify-content-around">
								<h5 class="card-title">Produit : AmazFit <br /><strong>Prix : 120 uacT</strong></h5>
								<!--h5 class="card-title"><b>Prix : 120 uacC</b></h5-->
							</div>
							<!--  <p class="card-text">Description</p>  -->
							<div class="text-center">
								<a href="/shop/buy?name=AmazFit&price=120&image=amazfitGTR"><button class="btn btn-primary text-center">Acheter</button></a>
							</div>
					</div>
				</div>
				<div class="card" style="width: 18rem;">
					<div class="container-fluid"> <img src="/img/bose700.jpg" class="card-img-top" alt="..." width="120" height="300"></div>
					<div class="card-body">
							<div class="d-flex justify-content-around">
								<h5 class="card-title">Produit : Bose 700 <br /><strong>Prix : 275 uacT</strong></h5>
								<!--h5 class="card-title"><b>Prix : 275 uacC</b></h5-->
							</div>
							<!--  <p class="card-text">Description</p>  -->
							<div class="text-center">
								<a href="/shop/buy?name=Boose-700&price=275&image=bose700"><button class="btn btn-primary text-center">Acheter</button></a>
							</div>
					</div>
				</div>
				<div class="card" style="width: 18rem;">
					<div class="container-fluid"> <img src="/img/ssd2to.jpg" class="card-img-top" alt="..." width="120" height="300"></div>
					<div class="card-body">
							<div class="d-flex justify-content-around">
								<h5 class="card-title">Produit : SSD 2To <br /><strong>Prix : 400 uacT</strong></h5>
								<!--h5 class="card-title"><b>Prix : 400 uacC</b></h5-->
							</div>
							<!--  <p class="card-text">Description</p>  -->
							<div class="text-center">
								<a href="/shop/buy?name=SSD-2-To&price=400&image=ssd2to"><button class="btn btn-primary text-center">Acheter</button></a>
							</div>
					</div>
				</div>
			</div>
			
			

    </div>
	</body>
</html>