package com.ifri.controllers;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ifri.classes.Block;
import com.ifri.classes.TransOutput;
import com.ifri.classes.Transaction;
import com.ifri.classes.Wallet;
import com.ifri.main.Application;
import com.ifri.models.Transfert;

@Controller
@RequestMapping("/transfert")
public class TransfertController {

	@RequestMapping(value = "/View")
	public String View(Model model) throws IOException, Exception {
		//Transfert transfert = new Transfert();
		
		//get listUTXO from file to Application.listUTXO
		Application.updateListUTXOVariable();
		
		
		float solde = 0;
		JSONArray infos = Application.getDatasFromFile("infos");
		JSONObject info  = (JSONObject) infos.get(0);
		Wallet wallet = new Wallet();
		
		wallet.setFullname((String)info.get("fullname"));
		wallet.setEmail((String)info.get("email"));
		wallet.setPassword((String)info.get("password"));
		wallet.setPrivateKey( (PrivateKey) Application.getPrivateKey((String)info.get("privateKey")) );
		wallet.setPublicKey( (PublicKey)Application.getPublicKey((String)info.get("publicKey")));
		
		model.addAttribute("solde", wallet.calculateBalance());
		//model.addAttribute("transfert", transfert);
		
		return "transfert";
	}
	
	@RequestMapping(value = "/treat")
	public String treat(@RequestParam(required=false) String recipient,float value,Model model) throws IOException, Exception {

		//get listUTXO from file to Application.listUTXO
		Application.updateListUTXOVariable();
		
		//JSONArray products = Application.getDatasFromFile("products");
		JSONArray infos = Application.getDatasFromFile("infos");
		JSONObject info  = (JSONObject) infos.get(0);
		
		Wallet wallet = new Wallet();
		
		wallet.setFullname((String)info.get("fullname"));
		wallet.setEmail((String)info.get("email"));
		wallet.setPassword((String)info.get("password"));
		wallet.setPrivateKey( (PrivateKey) Application.getPrivateKey((String)info.get("privateKey")) );
		wallet.setPublicKey( (PublicKey)Application.getPublicKey((String)info.get("publicKey")));
		//wallet.listUTXOWallet = (HashMap<String, TransOutput>)info.get("listUTXOWallet");
		System.out.println("wallet current: " + wallet);
		
		
      	if(wallet.calculateBalance() >= value) {
      		//System.out.println("value :" + value); 
      		//System.out.println("receiver :" + recipient);
      		
      		//get public key of the admin
    		JSONObject adminDatas = (JSONObject) Application.readJson("admin");
    		PublicKey pubKeyAdmin = Application.getPublicKey((String)adminDatas.get("publicKey"));
    		//get publicKey recipient in PublicKey type
    		PublicKey pubKeyRecipient = Application.getPublicKey(recipient);
    		
      		float gains = (value/100)*3;
      		float rest = value - gains;
      		//the new transaction for payment
    		Transaction transaction = wallet.sendMoney( pubKeyRecipient, rest);
    		//the new transaction for the admin
    		Transaction transactionGain = wallet.sendMoney( pubKeyAdmin, gains);
    		
    		
	        //get blockchain from file to Application.blockchain
	      	Application.updateBlockchainVariable();
	      	//add transaction
	        if(Application.blockchain.size() > 0) {
	    		Block currentBlock = Application.blockchain.get(Application.blockchain.size()-1);
	    		
	    		//check the limit of 10 transactions by block
	    		if(currentBlock.transactions.size() < 10) {
	    			currentBlock.addTransaction(transaction);
	    			currentBlock.mining(Application.difficulty);
	    			Application.blockchain.set((Application.blockchain.size()-1), currentBlock);
	    		}else {
	    			Block block = new Block(currentBlock.getPreviousHash());
	        		block.addTransaction(transaction);
	        		block.mining(Application.difficulty);
	        		Application.blockchain.add(block);
	    		}
	    	}else {
	    		//System.out.println("transaction: " + transaction);

	    		//if no blocks in the array
	    		Block block = new Block("0");
	    		//System.out.println("blockA: " + block);
	    		block.addTransaction(transaction);
	    		block.mining(Application.difficulty);
	    		//System.out.println("blockB: " + block);
	    		Application.blockchain.add(block);
	    	}
	        //add transactionGains
	        if(Application.blockchain.size() > 0) {
	    		Block currentBlock = Application.blockchain.get(Application.blockchain.size()-1);
	    		
	    		//check the limit of 10 transactions by block
	    		if(currentBlock.transactions.size() < 10) {
	    			currentBlock.addTransaction(transactionGain);
	    			currentBlock.mining(Application.difficulty);
	    			Application.blockchain.set((Application.blockchain.size()-1), currentBlock);
	    		}else {
	    			Block block = new Block(currentBlock.getPreviousHash());
	        		block.addTransaction(transactionGain);
	        		block.mining(Application.difficulty);
	        		Application.blockchain.add(block);
	    		}
	    	}else {
	    		//System.out.println("transaction: " + transaction);

	    		//if no blocks in the array
	    		Block block = new Block("0");
	    		//System.out.println("blockA: " + block);
	    		block.addTransaction(transactionGain);
	    		block.mining(Application.difficulty);
	    		//System.out.println("blockB: " + block);
	    		Application.blockchain.add(block);
	    	}

	        //update blockchain from Application.blockchain to file
	        Application.updateBlockchainFIle();
	        //update listUTXO from Application.listUTXO to file
	        Application.updateListUTXOFIle();
	        
      	}else {
      		model.addAttribute("msg", "Vous n\'avez pas assez de fonds !!!");
    		return "transfert";
      	}
		return "/home";
	}
	
}
